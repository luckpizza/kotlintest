package com.example.revolutexample.data.network

import android.util.Log
import com.example.revolutexample.entities.Currency
import com.example.revolutexample.entities.ExchangeRate
import com.example.revolutexample.entities.ExchangeRates

data class RatesDao (
    var base: String,
    var rates: Map<String, Double>) {

    internal fun toExchangeRate(): ExchangeRates {
        return ExchangeRates(rates.map {
            Log.d("RatesDao", it.key + it.value)
            ExchangeRate(Currency(it.key), it.value)
        })

    }
}
