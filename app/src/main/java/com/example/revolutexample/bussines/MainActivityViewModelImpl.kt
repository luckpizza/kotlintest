package com.example.revolutexample.bussines

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.blongho.country_data.World
import com.example.revolutexample.data.CurrenciesBackendNetwork
import com.example.revolutexample.entities.Currency
import com.example.revolutexample.entities.ExchangeRate
import com.example.revolutexample.entities.ExchangeRates
import com.example.revolutexample.ui.MainActivityViewModel
import com.example.revolutexample.ui.UiCurrencyConversionsState
import com.example.revolutexample.ui.UiEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal


class MainActivityViewModelImpl(private val  backend: CurrenciesBackend = CurrenciesBackendNetwork()) : MainActivityViewModel, ViewModel() {

    override val uiState: LiveData<List<UiCurrencyConversionsState>>
        get() = _values
    override val uiEvents: LiveData<UiEvent>
        get() = _uiEvents


    private val _uiEvents = MutableLiveData<UiEvent>()
    private val _values = MutableLiveData<List<UiCurrencyConversionsState>>()
    private var currentSelectedCurrency: Currency = Currency.DEFAULT
    private var currentSelectedAmount: BigDecimal = BigDecimal.ONE
    private val disposables by lazy { CompositeDisposable() }


    init {
        updateDominantCurrencyValue()
    }

    override fun updateAmountForCurrency(
        newAmount: String,
        uiCurrencyConversionsState: UiCurrencyConversionsState
    ) {
        if(uiCurrencyConversionsState.currencyIso3 == currentSelectedCurrency.iso3
            && _values.value?.size != 0 && _values.value?.get(0)?.formattedAmount  != newAmount ) {
            setAsDominantCurrency(uiCurrencyConversionsState, newAmount)
        }
    }

    override fun setAsDominantCurrency(newMainCurrencyConversionsState: UiCurrencyConversionsState, amount: String) {
        val scaledAmount = amount.convertToBigDecimal()
        if(currentSelectedCurrency.iso3 != newMainCurrencyConversionsState.currencyIso3
            || scaledAmount.compareTo( currentSelectedAmount) != 0) {
            disposables.clear()
            currentSelectedCurrency = Currency(newMainCurrencyConversionsState.currencyIso3)
            currentSelectedAmount = scaledAmount
            updateDominantCurrencyValue()
            _uiEvents.postValue(UiEvent.MainCurrencyChangeTriggered)
        }
    }

    override fun refreshRates() {
        disposables +=  updateDominantCurrencyValue()
    }

    @SuppressLint("CheckResult")
    private fun updateDominantCurrencyValue(): Disposable {
        _uiEvents.postValue(UiEvent.Loading)
        return backend.getExchangeRates(currentSelectedCurrency)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
            onSuccess = {
                updateValuesWithNewExchanges(it)},
            onError = {
                _uiEvents.postValue(UiEvent.ErrorLoading)
            }
        )
    }


    private fun updateValuesWithNewExchanges(newExchangesList: ExchangeRates) {
        val exchangesList = newExchangesList.exchanges.map {  it.toUiCurrency(currentSelectedAmount)}.toMutableList()
        exchangesList[0] = UiCurrencyConversionsStateImpl(currentSelectedAmount.toString(), currentSelectedCurrency.iso3)
        _values.postValue(exchangesList)
    }

}

private fun String?.convertToBigDecimal(): BigDecimal =
    if (this == null || isEmpty() || isBlank()) {
        BigDecimal.ZERO
    } else {
        toBigDecimal().setScale(2,BigDecimal.ROUND_CEILING )
    }

private fun ExchangeRate.toUiCurrency(currentSelectedAmount: BigDecimal): UiCurrencyConversionsState {
    return UiCurrencyConversionsStateImpl(currentSelectedAmount.multiply(rate.toBigDecimal()).setScale(2, BigDecimal.ROUND_CEILING).toString(), currency.iso3)
}

class UiCurrencyConversionsStateImpl(
    override val formattedAmount: String,
    override val currencyIso3: String
) : UiCurrencyConversionsState {
    override val currencyFlag: Int
        get() = World.getFlagOf(World.getAllCurrencies().find { it.code == currencyIso3 }!!.country)
    override val currencyName: String
        get() = World.getAllCurrencies().find { it.code == currencyIso3 }!!.name
}
