package com.example.revolutexample.entities

data class ExchangeRates(val exchanges: List<ExchangeRate>)

data class ExchangeRate(val currency: Currency, val rate: Double)
