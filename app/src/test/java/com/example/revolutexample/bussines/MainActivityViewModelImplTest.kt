package com.example.revolutexample.bussines

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.revolutexample.entities.Currency
import com.example.revolutexample.entities.ExchangeRates
import com.example.revolutexample.rules.RxTestSchedulerRule
import com.nhaarman.mockitokotlin2.*
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MainActivityViewModelImplTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()
    @Rule
    @JvmField
    val rxRule = RxTestSchedulerRule()

    val  backend: CurrenciesBackend = mock()
    lateinit var testeable: MainActivityViewModelImpl
    val singleResponse: Single<ExchangeRates> = mock()

    @Before
    fun setup() {
        whenever(backend.getExchangeRates(any())).thenReturn(singleResponse)
        testeable = MainActivityViewModelImpl(backend = backend)
    }

    @Test
    fun `should start with EUR as default currency`() {
        verify(backend).getExchangeRates(Currency.DEFAULT)
        verifyNoMoreInteractions(backend)
    }

    @Test
    fun `should request exchanges 2 time for EUR currency, once at startup and one when EUR is setAsDominant`() {
        // When
        testeable.setAsDominantCurrency(UiCurrencyConversionsStateImpl("0.2", "EUR"), "0.2")
        // Then
        argumentCaptor<Currency>().apply {
            verify(backend, times(2)).getExchangeRates(capture())
            assert(allValues.first().iso3 == "EUR")
            assert(allValues[1].iso3 == "EUR")
        }
    }

    @Test
    fun `should request exchanges 2 times, one Default currency at startup and USD second when USD is setAsDominant`() {

        // Given: Request of default currency
        argumentCaptor<Currency>().apply {
            verify(backend, times(1)).getExchangeRates(capture())
            assert(allValues.first().iso3 == "EUR")
        }
        // When
        testeable.setAsDominantCurrency(UiCurrencyConversionsStateImpl("0.2", "USD"), "0.2")
        // Then
        argumentCaptor<Currency>().apply {
            verify(backend, times(2)).getExchangeRates(capture())
            assert(allValues.first().iso3 == "EUR") //Default
            assert(allValues[1].iso3 == "USD")
        }
    }

    @Test
    fun `should ignore updateAmount request from currencies that are not set as Dominant, when Dominant is the  Default`() {
        // Given: Request of default currency
        argumentCaptor<Currency>().apply {
            verify(backend, times(1)).getExchangeRates(capture())
            assert(allValues.first().iso3 == "EUR")
        }
        //When
        testeable.updateAmountForCurrency("0.5", UiCurrencyConversionsStateImpl("0.2", "USD"))

        //Then
        verifyNoMoreInteractions(backend)
    }

    @Test
    fun `should ignore updateAmount() calls from currencies that are not set as Dominant, when Dominant is USD`() {
        // Given: Request of default currency
        argumentCaptor<Currency>().apply {
            verify(backend, times(1)).getExchangeRates(capture())
            assert(allValues.first().iso3 == "EUR")
        }
        //When
        testeable.setAsDominantCurrency(UiCurrencyConversionsStateImpl("0.2", "USD"), "0.2")

        testeable.updateAmountForCurrency("0.5", UiCurrencyConversionsStateImpl("0.2", "EUR"))

        //Then
        argumentCaptor<Currency>().apply {
            verify(backend, times(2)).getExchangeRates(capture())
            assert(allValues.first().iso3 == "EUR")
            assert(allValues[1].iso3 == "USD")

        }
    }

    @Test
    fun `should request new exchanges when updateAmount() is called with currency that is set as Dominant, when Dominant is USD`() {
        // Given: Request of default currency
        argumentCaptor<Currency>().apply {
            verify(backend, times(1)).getExchangeRates(capture())
            assert(allValues.first().iso3 == "EUR")
        }
        //When
        testeable.setAsDominantCurrency(UiCurrencyConversionsStateImpl("0.2", "USD"), "0.2")

        testeable.updateAmountForCurrency("0.5", UiCurrencyConversionsStateImpl("0.3", "USD"))

        //Then
        argumentCaptor<Currency>().apply {
            verify(backend, times(3)).getExchangeRates(capture())
            assert(allValues.first().iso3 == "EUR")
            assert(allValues[1].iso3 == "USD")
            assert(allValues[2].iso3 == "USD")
        }
    }
}