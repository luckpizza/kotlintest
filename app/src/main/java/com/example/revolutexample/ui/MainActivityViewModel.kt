package com.example.revolutexample.ui

import androidx.lifecycle.LiveData

interface MainActivityViewModel {

    fun updateAmountForCurrency(
        newAmount: String,
        uiCurrencyConversionsState: UiCurrencyConversionsState
    )
    fun setAsDominantCurrency(newMainCurrencyConversionsState: UiCurrencyConversionsState, amount: String)
    fun refreshRates()
    val uiState: LiveData<List<UiCurrencyConversionsState>>
    val uiEvents: LiveData<UiEvent>
}

sealed class UiEvent {
    object Loading: UiEvent()
    object MainCurrencyChangeTriggered: UiEvent()
    object ErrorLoading: UiEvent()
}

interface UiCurrencyConversionsState{
    val formattedAmount: String
    val currencyIso3: String
    val currencyFlag: Int
    val currencyName: String
}
