package com.example.revolutexample.utils

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.*

inline fun <reified VIEW_MODEL : ViewModel> FragmentActivity.bindViewModel(crossinline factory: () -> ViewModelProvider.Factory) =
    lazy {
        ViewModelProviders.of(this, factory.invoke())[VIEW_MODEL::class.java]
    }


fun <T> LiveData<T>.nonNullObserve(owner: LifecycleOwner, observer: (t: T) -> Unit) {
    this.observe(owner, Observer {
        it?.let(observer)
    })
}
