package com.example.revolutexample.data

import android.util.Log
import com.example.revolutexample.bussines.CurrenciesBackend
import com.example.revolutexample.data.network.Api
import com.example.revolutexample.data.network.RatesDao
import com.example.revolutexample.entities.Currency
import com.example.revolutexample.entities.ExchangeRate
import com.example.revolutexample.entities.ExchangeRates
import com.google.gson.Gson
import io.reactivex.Single
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class CurrenciesBackendNetwork: CurrenciesBackend {
    val api: Api = provideFeedApi(OkHttpClient(), Gson())

    override fun getExchangeRates(baseCurrency: Currency): Single<ExchangeRates> {
        return api.getAvailability(baseCurrency.iso3).map {
            Log.d("getExchangeRates", "${it.base} + ${it.rates}")
            it.toExchangeRate() }
    }

    private fun provideFeedApi(okHttpClient: OkHttpClient, gson: Gson): Api {
        return Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .baseUrl("https://revolut.duckdns.org")
            .build()
            .create(Api::class.java)
    }
}

