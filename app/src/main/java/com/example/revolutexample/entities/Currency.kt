package com.example.revolutexample.entities

class Currency(val iso3: String) {
    companion object {
        val DEFAULT = Currency("EUR")
    }
}

