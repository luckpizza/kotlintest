package com.example.revolutexample

import android.app.Application
import com.blongho.country_data.World

class RevolutExampleApp : Application() {

    override fun onCreate() {
        super.onCreate()
        World.init(applicationContext)
    }
}