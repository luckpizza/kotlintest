package com.example.revolutexample.bussines

import com.example.revolutexample.entities.Currency
import com.example.revolutexample.entities.ExchangeRates
import io.reactivex.Single

interface  CurrenciesBackend {
    fun getExchangeRates(baseCurrency: Currency): Single<ExchangeRates>
}
