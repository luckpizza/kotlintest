package com.example.revolutexample.ui.exchangelist

import android.text.Editable
import android.text.TextWatcher
import android.view.MotionEvent
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.revolutexample.ui.MainActivityViewModel
import com.example.revolutexample.ui.UiCurrencyConversionsState
import kotlinx.android.synthetic.main.currency_row.view.*

class UiCurrencyViewHolder(view: View, private val viewModel: MainActivityViewModel) : RecyclerView.ViewHolder(view) {

    private var itemState: UiCurrencyConversionsState? = null
    private val textChangeListener = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) { }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

        override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
            itemState?.let {
                viewModel.updateAmountForCurrency(text.toString(), it)
            }
        }
    }

    init {
        itemView.vAmount.addTextChangedListener(textChangeListener)
    }

    fun bind(uiCurrencyConversionsState: UiCurrencyConversionsState) {
        itemState = uiCurrencyConversionsState
        itemView.vCurrencyIcon.setImageDrawable(itemView.context.getDrawable(uiCurrencyConversionsState.currencyFlag))
        itemView.vCurrencyIso.text = uiCurrencyConversionsState.currencyIso3
        itemView.vCurrencyName.text = uiCurrencyConversionsState.currencyName
        itemView.vAmount.text.replace(0, itemView.vAmount.text.length, uiCurrencyConversionsState.formattedAmount)
        itemView.setOnClickListener { _ -> viewModel.setAsDominantCurrency(uiCurrencyConversionsState, itemView.vAmount.text.toString())  }
        itemView.vAmount.setOnTouchListener(View.OnTouchListener { _, event ->
            if (MotionEvent.ACTION_UP == event.action) {
                viewModel.setAsDominantCurrency(
                    uiCurrencyConversionsState,
                    itemView.vAmount.text.toString()
                )
            }
            false
        })
    }
}
