package com.example.revolutexample.rules

import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.TestScheduler
import org.junit.rules.ExternalResource

// Inspired by https://github.com/airbnb/MvRx/blob/master/testing/src/main/kotlin/com/airbnb/mvrx/test/MvRxTestRule.kt
class RxTestSchedulerRule : ExternalResource() {

    val testScheduler = TestScheduler()

    override fun before() {
        RxAndroidPlugins.reset()
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.reset()
        RxJavaPlugins.setNewThreadSchedulerHandler { testScheduler }
        RxJavaPlugins.setComputationSchedulerHandler { testScheduler }
        RxJavaPlugins.setIoSchedulerHandler { testScheduler }
        RxJavaPlugins.setSingleSchedulerHandler { testScheduler }
    }

    override fun after() {
        RxAndroidPlugins.reset()
        RxJavaPlugins.reset()
    }
}