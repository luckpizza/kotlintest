package com.example.revolutexample.data.network

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET("/latest")
    fun getAvailability(@Query("base") currencyIso: String): Single<RatesDao>
}