package com.example.revolutexample.ui

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.postDelayed
import androidx.core.view.postOnAnimationDelayed
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.example.revolutexample.R
import com.example.revolutexample.bussines.MainActivityViewModelImpl
import com.example.revolutexample.ui.exchangelist.ExchangeRatesAdapter
import com.example.revolutexample.utils.bindViewModel
import com.example.revolutexample.utils.nonNullObserve
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var adapter: ExchangeRatesAdapter
    private val viewModel: MainActivityViewModel by bindViewModel<MainActivityViewModelImpl> { ViewModelProvider.AndroidViewModelFactory(application) }
    private var isMainCurrencyChangeTriggered = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        prepareExchangeRatesList()
        setupUiStateObserver()
        setUpUiEventsObserver()
    }

    override fun onResume() {
        super.onResume()
        postRefreshInTheFuture()
    }

    private fun setUpUiEventsObserver() {
        viewModel.uiEvents.nonNullObserve(this) {
            when (it) {
                UiEvent.Loading -> showLoadingIfNeeded()
                UiEvent.ErrorLoading -> showErrorAsNeeded()
                UiEvent.MainCurrencyChangeTriggered -> isMainCurrencyChangeTriggered = true
            }
        }
    }

    private fun setupUiStateObserver() {
        viewModel.uiState.nonNullObserve(this) {
            renderListView(it)
            if (isMainCurrencyChangeTriggered) {
                isMainCurrencyChangeTriggered = false
                vRecyclerView.postDelayed( {
                    vRecyclerView.smoothScrollToPosition(0)
                    vRecyclerView.preserveFocusAfterLayout
                }, 400)
            }
        }
    }

    private fun showErrorAsNeeded() {
        Toast.makeText(this, "Error loading exchange rates, probably should be handled better", Toast.LENGTH_LONG).show()
    }

    private fun showLoadingIfNeeded() {
        if(isMainCurrencyChangeTriggered) {
            Toast.makeText(this, "Loading rates ...", Toast.LENGTH_LONG).show()
        }
    }

    private fun prepareExchangeRatesList() {
        vRecyclerView.layoutManager = LinearLayoutManager(this)
        adapter = ExchangeRatesAdapter(viewModel, object : DiffUtil.ItemCallback<UiCurrencyConversionsState>() {
            override fun areItemsTheSame(oldItem: UiCurrencyConversionsState, newItem: UiCurrencyConversionsState): Boolean =
                oldItem.currencyIso3 == newItem.currencyIso3


            override fun areContentsTheSame(oldItem: UiCurrencyConversionsState, newItem: UiCurrencyConversionsState): Boolean =
                oldItem.currencyFlag == newItem.currencyFlag
                        && oldItem.formattedAmount == newItem.formattedAmount
        })
        vRecyclerView.adapter = adapter
        (vRecyclerView.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
    }

    private fun renderListView(list: List<UiCurrencyConversionsState>) {
        adapter.submitList(list.toMutableList())
    }

    private fun postRefreshInTheFuture() {
        vRecyclerView.postDelayed({postRefreshInTheFuture()}, 1000)
        viewModel.refreshRates()
    }
}