package com.example.revolutexample.ui.exchangelist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.revolutexample.R
import com.example.revolutexample.ui.MainActivityViewModel
import com.example.revolutexample.ui.UiCurrencyConversionsState

class ExchangeRatesAdapter(private val viewModel: MainActivityViewModel, diffCallback: DiffUtil.ItemCallback<UiCurrencyConversionsState>): ListAdapter<UiCurrencyConversionsState, UiCurrencyViewHolder>(diffCallback) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UiCurrencyViewHolder
            = UiCurrencyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.currency_row, parent, false), viewModel)


    override fun onBindViewHolder(holder: UiCurrencyViewHolder, position: Int) {
        holder.bind(getItem(position))
        if(position == 0) {
            holder.itemView.requestFocus()
        }
    }

    override fun submitList(list: MutableList<UiCurrencyConversionsState>?) {
        list?.let {
            if(it.size > 0 && itemCount > 0 && it[0].currencyIso3 == getItem(0)?.currencyIso3){
                it[0] = getItem(0)
            }
        }
        super.submitList(list)
    }
}